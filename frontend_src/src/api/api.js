import axios from 'axios'

const api = axios.create({
  baseURL: 'https://mail.kylejoeckel.com/api'
})

api.interceptors.response.use(response => response,
  (err) => {
    if (err.response.status === 401) {
      console.log(err)
    }
    return Promise.reject(err)
  })

export default api
