import api from './api'

export function sendMessage (message) {
  return api.post(`/mailer`, {
    message
  })
}
