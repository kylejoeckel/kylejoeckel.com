var express = require('express');
// var history = require('connnect-history-api-fallback');
const app = express();
const cors = require('cors');


app.use(express.json());

app.use(cors());

app.use(express.static('dist'));
// ########################################################
app.listen(process.env.PORT || 5000, (req, res) => {
    console.log('Listening on port 5000....');
});